def peindre(plateau, pos, direction, couleur, reserve, distance_max, peindre_murs=False):
    """ Peint avec la couleur les cases du plateau à partir de la position pos dans
        la direction indiquée en s'arrêtant au premier mur ou au bord du plateau ou
        lorsque que la distance maximum a été atteinte.

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire (lig,col) de int
        direction (str): un des caractères 'N','S','E','O' indiquant la direction de peinture
        couleur (str): une lettre indiquant l'idenfiant du joueur qui peint (couleur de la peinture)
        reserve (int): un entier indiquant la taille de la reserve de peinture du joueur
        distance_max (int): un entier indiquant la portée maximale du pistolet à peinture
        peindre_mur (bool): un booléen indiquant si on peint aussi les murs ou non

    Returns:
        dict: un dictionnaire avec 4 clés
                "cout": un entier indiquant le cout en unités de peinture de l'action
                "nb_repeintes": un entier indiquant le nombre de cases qui ont changé de couleur
                "nb_murs_repeints": un entier indiquant le nombre de murs qui ont changé de couleur
                "joueurs_touches": un ensemble (set) indiquant les joueurs touchés lors de l'action
    """
    res={"cout":0,
        "nb_repeintes":0,
        "nb_murs_repeints":0,
        "joueurs_touches":set()
    }

    for _ in range(distance_max):
        if est_sur_le_plateau(plateau, (pos[0], pos[1])): 
            if case.get_couleur(plateau[pos]) == " " :    
                
                if reserve > 0:                
                    if case.est_mur(plateau[pos]):
                        if peindre_murs:
                            res["nb_murs_repeints"]+=1
                        else:
                            return res
                    res["nb_repeintes"]+=1
                    res["cout"]+=1
                    reserve-=1
                    if case.get_joueurs(plateau[pos]):
                        for elem in case.get_joueurs(plateau[pos]):
                            res["joueurs_touches"].add(elem)
                else:
                    return res
            elif case.get_couleur(plateau[pos]) == couleur:

                if reserve > 0:
                    res["cout"]+=1
                    reserve-=1     
                    if case.get_joueurs(plateau[pos]):
                        for elem in case.get_joueurs(plateau[pos]):
                            res["joueurs_touches"].add(elem) 
                else:
                    return res            
                
            else:         
                
                if reserve > 1:   
                    if case.get_joueurs(plateau[pos]):
                        for elem in case.get_joueurs(plateau[pos]):
                            res["joueurs_touches"].add(elem)        
                    if case.est_mur(plateau[pos]):
                        if peindre_murs:
                            res["nb_murs_repeints"]+=1
                        else:
                            return res
                    res["nb_repeintes"]+=1
                    res["cout"]+=2
                    reserve-=2  
                else:
                    return res
            case.peindre(plateau[pos],couleur)
            pos = (pos[0]+INC_DIRECTION[direction][0],pos[1]+INC_DIRECTION[direction][1])
            
    return res 
